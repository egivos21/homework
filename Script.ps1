$logfilepath="C:\LogFile.txt"
function WriteToLogFile ($message)
	{
	(Get-Date).ToString() +" - "+ $message >> $logfilepath
	}
if(Test-Path $logfilepath)
	{
	Remove-Item $logfilepath
	}
	
$url1 = 'https://gitlab.com/egivos21/homework/-/raw/master/config.json?inline=false'
$dest1 = 'C:\Users\vagrant\Downloads\config.json'
$url2 = 'https://gitlab.com/egivos21/homework/-/raw/master/iisstart.htm?inline=false'
$dest2 = 'C:\Users\vagrant\Downloads\iisstart.htm'
$web = New-Object -TypeName System.Net.WebClient
$Desc = Get-WmiObject -class Win32_OperatingSystem
$adapt = Get-NetAdapter -Name "Ethernet" | Select-Object InterfaceAlias , InterfaceIndex
$Password = "vagrant"  | ConvertTo-SecureString -AsPlainText -Force
$hostname = hostname

try {
$web.DownloadFile($url1,$dest1)
$config = Get-Content -Path 'C:\Users\vagrant\Downloads\config.json' | ConvertFrom-Json
}
catch {
WriteToLogFile $Error
}
try {
Set-TimeZone -Id "E. Europe Standard Time" -PassThru
}
catch {
WriteToLogFile $Error
}
try {
set-DnsClientServerAddress -InterfaceIndex $adapt.InterfaceIndex -ServerAddresses ($config.dns1,$config.dns2)
}
catch {
WriteToLogFile $Error
}
try {
Install-WindowsFeature -Name $config.role -IncludeAllSubFeature -ComputerName $hostname | Out-Null
}
catch {
WriteToLogFile $Error
}
try {
$web.DownloadFile($url2,$dest2)
}
catch {
WriteToLogFile $Error
}
try {
Copy-Item $dest2 -Destination 'C:\inetpub\wwwroot' -Recurse
}
catch {
WriteToLogFile $Error
}
try {
Set-ExecutionPolicy Bypass -Scope Process -Force; iwr https://community.chocolatey.org/install.ps1 -UseBasicParsing | iex
}
catch {
WriteToLogFile $Error
}
try {
choco install $config.packages -y
}
catch {
WriteToLogFile $Error
}
secedit /export /cfg c:\secpol.cfg
(gc C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg
secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg /areas SECURITYPOLICY
rm -force c:\secpol.cfg -confirm:$false
try {
New-LocalUser $config.owner -Password $Password -FullName "Egidijus Vosylius" -Description "Candidate to Adform"
}
catch {
WriteToLogFile $Error
}
try {
Add-LocalGroupMember -Group "Administrators" -Member $config.owner
}
catch {
WriteToLogFile $Error
}
try {
if($config.firewall -eq "disabled"){Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False}else {write-host(Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled True)}
}
catch {
WriteToLogFile $Error
}
try {
Rename-Computer -NewName $config.hostname
}
catch {
WriteToLogFile $Error
}
try {
$Desc.Description="Adform Web Server"
$Desc.put()
}
catch {
WriteToLogFile $Error
}
try {
Add-Computer -WorkgroupName $config.environment
}
catch {
WriteToLogFile $Error
}

Restart-Computer
